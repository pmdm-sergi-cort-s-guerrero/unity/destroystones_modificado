﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InterfaceAwake : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1.0f;
        GameManager.currentNumberStonesDestroyed = 0;
        GameManager.currentNumberStonesThrown = 0;
        GameManager.currentNumberChickensAlive = 5;
    }

    public void Click()
    {
        SceneManager.LoadScene(1,LoadSceneMode.Single);
    }
}