﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stone : MonoBehaviour
{
    public GameObject explosion;

    public AudioClip destroyed;
    public AudioClip destroyedByUser;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Earth"))
        {
            DestroyStone();
            AudioSource.PlayClipAtPoint(destroyed, new Vector3(0,1,0),0.4f);
        }
    }

    private void OnMouseDown()
    {
        DestroyStone();
        AudioSource.PlayClipAtPoint(destroyedByUser, new Vector3(0,1,0),0.8f);
        GameManager.currentNumberStonesDestroyed++;
        Time.timeScale += 0.1f;
    }

    private void DestroyStone()
    {
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}