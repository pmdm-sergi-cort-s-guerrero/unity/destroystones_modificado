﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using UnityEngine.SceneManagement;

public class MainLoop: MonoBehaviour 
{
	public GameObject[] stones = new GameObject[3];
	public float torque = 5.0f;
	public float minAntiGravity = 20.0f, maxAntiGravity = 40.0f;
	public float minLateralForce = -15.0f, maxLateralForce = 15.0f;
	public float minTimeBetweenStones = 1f, maxTimeBetweenStones = 3f;
	public float minX = -30.0f, maxX = 30.0f;
	public float minZ = -5.0f, maxZ = 20.0f;
	
	private bool enableStones = true;
	private Rigidbody rigidbody;
	
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(ThrowStones());
		StartCoroutine(RemoveExplosions());
	}

	private IEnumerator RemoveExplosions()
	{
		while (true)
		{
			yield return new WaitForSeconds(60.0f);

			GameObject[] toWipeOut = GameObject.FindGameObjectsWithTag("Explosion");
			for (var i = 0; i < toWipeOut.Length/2; i++)
				Destroy(toWipeOut[i]);
		}
	}

	// Update is called once per frame
	void Update () {
	}

	IEnumerator ThrowStones()
	{
		// Initial delay
		yield return new WaitForSeconds(2.0f);
		
		while(enableStones)
		{
			if (GameManager.currentNumberChickensAlive == 0)
				SceneManager.LoadScene(2);
			
			GameObject stone = (GameObject) Instantiate(stones[Random.Range(0, stones.Length)]);
			stone.transform.position = new Vector3(Random.Range(minX, maxX), 70.0f, Random.Range(minZ, maxZ));
			stone.transform.rotation = Random.rotation;

			rigidbody = stone.GetComponent<Rigidbody>();
			
			rigidbody.AddTorque(Vector3.up * torque, ForceMode.Impulse);
			rigidbody.AddTorque(Vector3.right * torque, ForceMode.Impulse);
			rigidbody.AddTorque(Vector3.forward * torque, ForceMode.Impulse);
			
			rigidbody.AddForce(Vector3.up * Random.Range(minAntiGravity, maxAntiGravity), ForceMode.Impulse);
			rigidbody.AddForce(Vector3.right * Random.Range(minLateralForce, maxLateralForce), ForceMode.Impulse);

			GameManager.currentNumberStonesThrown++;
			
			yield return new WaitForSeconds(Random.Range(minTimeBetweenStones, maxTimeBetweenStones));
		}
	}
}
