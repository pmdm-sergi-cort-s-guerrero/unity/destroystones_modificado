﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceStone : MonoBehaviour
{
    public Text textThrown;
    public Text textDestroyed;
    public Text textAlive;
    
    // Start is called before the first frame update
    void Start()
    {
        GameManager.currentNumberStonesThrown = 0;
        GameManager.currentNumberChickensAlive = 5;
        GameManager.currentNumberStonesDestroyed = 0;
    }

    // Update is called once per frame
    void Update()
    {
        textThrown.text = "Number of stones thrown: " + GameManager.currentNumberStonesThrown;
        textDestroyed.text = "Number of stones destroyed: "+GameManager.currentNumberStonesDestroyed;
        textAlive.text = GameManager.currentNumberChickensAlive + " alive";
    }
}