﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InterfaceFinal : MonoBehaviour
{
    public Text textThrown;
    public Text textDestroyed;
    
    // Start is called before the first frame update
    void Start()
    {
        textThrown.text = "Number of stones thrown: " + GameManager.currentNumberStonesThrown;
        textDestroyed.text = "Number of stones destroyed: "+GameManager.currentNumberStonesDestroyed;
    }

    public void Click()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}