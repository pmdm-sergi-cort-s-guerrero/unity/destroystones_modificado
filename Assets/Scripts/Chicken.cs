﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Chicken : MonoBehaviour
{
    public GameObject deadEffect;
    public AudioClip deadSound;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(HandleGrowth());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator HandleGrowth()
    {
        while (true)
        {
            yield return new WaitForSeconds(3.0f);
            gameObject.transform.localScale += new Vector3(0.1f,0.1f,0.1f)*(Random.value*2);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Stone"))
        {
            GameObject fire = Instantiate(deadEffect, transform.position, Random.rotation);
            fire.transform.localScale = gameObject.transform.localScale * 5;
            AudioSource.PlayClipAtPoint(deadSound, new Vector3(0,1,0),1.0f);
            Destroy(gameObject);
            Destroy(other.gameObject);
            GameManager.currentNumberChickensAlive -= 1;
        }
    }
}